<div align="center">
  <img src="images/lizard.jpg" alt="Trip App logo"/>
  <h3 align="center">Trip App</h3>
  <br><br><br>
</div>

## Trip App

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li><a href="#built-with">Built With</a></li>
    <li>
      <a href="#local-development">Local Development</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#client">Client</a></li>
        <li><a href="#api">API</a></li>
        <li><a href="#typescript">Typescript</a></li>
      </ul>
    </li>
    <li><a href="#ci">CI</a></li>
    <li><a href="#future-plans">Future plans</a></li>
  </ol>
</details>
<br><br><br>

## About The Project
The objective of this project was to create example Web Application for browsing Trips to different countries.

<p align="center">
  <img src="images/app-screenshot.png" alt="App screenshot"/>
</p>

## Built with
**Client application** is built with *React 17* and is placed in the `client` directory.

**Backend application** is a mocked local-development live server, created using *Node.js 16.16* with express and lowdb JavaScript libraries and is placed in the `backend` directory.

## Local Development

### Prerequisites
* Node.js version 16.16 with npm 8.11

### Bootstraping the project
To start local development run the following command in `client` and `backend` directories:

```bash
npm i
```
This will install all of the required packages.

### Backend
To start local mocked API live-server read the detailed [README.md](backend/README.md) or simply use the following command in `backend` directory:
```bash
npm run start
```

### Client
Provide correct values for env variables in `client/.env.development` file. Default values should work if you host **API** on the provided port.

To start the client app read the detailed [README.md](client/README.md) or simply run the following command in `client` directory:

```bash
npm run start
```

it will setup development environment and start a server, as well as hot module reloading.

### Typescript
We use typescript in our **client** Reactjs app. All of the types definitions are located in **libs/types/**

### CI
We use Gitlab CI for building and testing application on pushing any changes.

### Future plans
Next plans for this application are:
* Create reusable React hook for SSE listening
* Upgrade to React 18 after libraries like cypress & react-alert update their peer dependencies
* Add TypeScript to the backend
export type TripCountry = {
  id: string;
  country: string;
  description?: string;
  countryOverviewImagePath?: string;
  listImageUrl: string;
  detailImageUrl: string;
}
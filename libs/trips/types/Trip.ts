import { TripCountry } from './TripCountry';

export type Trip = {
  id: string;
  title: string;
  description: string;
  price: number;
  originalPrice: number;
  rating: number;
  duration: number; // In days
  countries: TripCountry[];
  listImageUrl: string;
  detailImageUrl: string;
  overviewImages: any[];
}
To run mock json-server you need to have 'json-server' installed:
```
npm install
```


To start live mock server use:
```
npm run start
```
It is possible to switch the default port in index.js file.

# Database file
We use lowdb library for mocking JSON database.
database.js file generates mocked database.json file.
Any changes made to this JSON file are sent to the client by SSE (server side events).
import Chance from 'chance';
import { title } from 'process';
import { join, dirname } from 'path';
import { Low, JSONFile } from 'lowdb';
import { fileURLToPath } from 'url';
import { v4 as uuidv4 } from 'uuid';

const chance = new Chance();

const COUNTRIES_AMOUNT = 5;
const TRIPS_AMOUNT = 10;
const OVERVIEW_IMAGES_AMOUNT = 5;

const generateCountries = (amount) => {
  const countries = [];
  for (let i=1; i<=amount; i++) {
    countries.push(
      generateRandomCountry()
    );
  }

  return countries;
}

function generateRandomCountry() {
  const country = chance.country({ full: true });
  return {
    id: uuidv4(),
    country,
    description: chance.paragraph({ sentences: 5 }),
    listImageUrl: getRandomImage(country, 600, 800),
    detailImageUrl: getRandomImage(country, 1900, 800),
    slug: getSlug(country),
  };
}

const generateTrips = (countries, amount) => {
  const trips = [];
  for (let i=1; i<=amount; i++) {
    trips.push(generateRandomTrip(countries));
  }

  return trips;
}

const generateRandomTrip = (countries) => {
  const title = chance.sentence({ words: chance.integer({min: 1, max: 2}) });
  const randomCountryName = chance.country({ full: true });
  return {
    id: uuidv4(),
    title,
    countries,
    description: chance.paragraph({ sentences: 5 }),
    listImageUrl: getRandomImage(randomCountryName, 600, 800),
    detailImageUrl: getRandomImage(randomCountryName, 1000, 600),
    price: chance.integer({ min: 300, max: 4000 }),
    originalPrice: chance.integer({ min: 4001, max: 5000 }),
    rating: chance.floating({ min: 4, max: 5, fixed: 1 }),
    duration: chance.integer({ min: 1, max: 100 }),
    slug: getSlug(title),
    overviewImages: generateOverviewImages()
  };
}

function generateOverviewImages() {
  const overviewImages = [];
  for (let i=1; i<=OVERVIEW_IMAGES_AMOUNT; i++) {
    const seed = chance.country({ full: true });
    overviewImages.push({imageUrl: getRandomImage(seed, 200, 200)})
  }
  return overviewImages;
}

function getRandomImage(tag, width=200, height=200) {
  return `https://source.unsplash.com/${width}x${height}/?${tag}`
}

function getSlug(text) {
  return title.toLowerCase().replaceAll(' ', '-');
}

function getData() {
  const data = { trips: [], countries: [] };
  
  const countries = generateCountries(COUNTRIES_AMOUNT);

  data.countries = countries;
  data.trips = generateTrips(countries, TRIPS_AMOUNT);
  return data
}

const __dirname = dirname(fileURLToPath(import.meta.url));
const DATABASE_FILE_NAME = 'database.json';
const DB_FILE_PATH = join(__dirname, DATABASE_FILE_NAME)

function instanciateDatabase() {
  const adapter = new JSONFile(DB_FILE_PATH)
  const db = new Low(adapter)
  db.data = getData();
  db.write();

  return db;
}

const db = instanciateDatabase();

export {db, generateRandomCountry, generateRandomTrip, DB_FILE_PATH};

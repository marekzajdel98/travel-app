import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import fs from 'fs';
import { db, generateRandomTrip, generateRandomCountry, DB_FILE_PATH } from './database.js';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const PORT = 3004;
const HTTP_SUCCESS_RESPONSE_CODE = 200;

app.listen(PORT, () => {
  console.log(`Mocked API listening at http://localhost:${PORT}`)
})

let clients = [];
// Handshake - keep SSE connection
function eventsHandler(req, res, next, resource) {
  const headers = {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache'
  };
  res.writeHead(HTTP_SUCCESS_RESPONSE_CODE, headers);

  const clientId = Date.now();

  const newClient = {
    id: clientId,
    res,
    resource
  };

  clients.push(newClient);

  req.on('close', () => {
    clients = clients.filter(client => client.id !== clientId);
  });

  next();
}

async function updateDatabaseObject() {
  await db.read()
}

// Update database object on each req
app.use(async (req, res, next) => {
  await updateDatabaseObject();
  next();
})


app.get('/countries-sse-handshake', (req, res, next) => {eventsHandler(req, res, next, 'countries')});
app.get('/trips-sse-handshake', (req, res, next) => {eventsHandler(req, res, next, 'trips')});

app.get('/trips', (req, res, next) => {
  const ids = req.query.ids;
  const countryId = req.query.country_id;
  let trips = db.data.trips;

  if (Array.isArray(ids)) {
    trips = filterAndSortByIds(trips, ids)
  } else if (ids) {
    const trip = getTripById(ids);
    if (trip) {
      trips = [trip];
    } else {
      trips = [];
    }
  }

  trips = countryId ? trips.filter(trip => trip.countries && trip.countries.some(c => c.id === countryId)) : trips;
  res.send(trips);
});

function getTripById(id) {
    return db.data.trips.find(t => t.id === id);
}

function filterAndSortByIds(items, ids=[]) {
  const filteredAndSortedItems = [];
  ids.forEach(id => {
    const trip = items.find(t => t.id === id);
    if (trip) filteredAndSortedItems.push(trip);
  })
  return filteredAndSortedItems;
}

app.get('/trips/:id', (req, res, next) => {
  const id = req.params.id
  res.send(db.data.trips.find(trip => trip.id == id));
});

app.get('/countries', (req, res, next) => {
  res.send(db.data.countries);
});

app.get('/countries/:id', (req, res, next) => {
  const id = req.params.id
  res.send(db.data.countries.find(country => country.id == id));
});

function sendCountriesToAll() {
  clients
    .filter(client => client.resource === 'countries')
    .forEach(client => client.res.write(`data: ${JSON.stringify({countries: db.data.countries})}\n\n`))
}

function sendTripsToAll() {
  clients
    .filter(client => client.resource === 'trips')
    .forEach(client => client.res.write(`data: ${JSON.stringify({trips: db.data.trips})}\n\n`))
}

async function addRandomTrip(req, res, next) {
  db.data.trips.push(generateRandomTrip())
  await db.write();
  res.send({success: true});
}

async function addRandomCountry(req, res, next) {
  db.data.countries.push(generateRandomCountry())
  await db.write();
  res.send({success: true});
}

async function removeLastTrip(req, res, next) {
  db.data.trips.pop();
  await db.write();
  res.send({success: true});
}

async function removeLastCountry(req, res, next) {
  db.data.countries.pop();
  await db.write();
  res.send({success: true});
}

app.post('/trips', addRandomTrip);
app.post('/countries', addRandomCountry);
app.delete('/trips', removeLastTrip);
app.delete('/countries', removeLastCountry);


const WATCH_FILE_INTERVAL_MS = 1000;
fs.watchFile(DB_FILE_PATH, { interval: WATCH_FILE_INTERVAL_MS }, async (curr, prev) => {
  console.log(`Db file Changed`);
  await updateDatabaseObject()
  sendTripsToAll();
  sendCountriesToAll();
});
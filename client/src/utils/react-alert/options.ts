import { transitions, positions } from 'react-alert'

export const REACT_ALERT_OPTIONS = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  transition: transitions.SCALE,
  containerStyle: {
    marginTop: '100px'
  }
}
export function moveArrayElementToTheBegginning(arr: any[], elementIndex: number) {
  arr.unshift(...arr.splice(elementIndex, 1));
  return arr;
}

/**
 * Generates a random number from 0 to ${max}
 * @param  {[number]} max
 * @return {[number]}
 */
export function generateRandomNumber(max: number) {
  if (max < 1) return 1;

  return Math.floor(Math.random() * (max + 1));
}

export const buildEventSource = (url: string) => {
  return new EventSource(url);
};
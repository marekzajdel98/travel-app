import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../../App';
import * as utils from '../../utils/helpers';
import * as api from '../../Api/Api';
import { darkTheme, DEFAULT_COLORS_MODE, lightTheme } from '../../services/Theme';

const mockApiFunctions = () => {
  const buildEventSourceSpy = jest.spyOn(utils, 'buildEventSource');
  const apiGetTripsSpy = jest.spyOn(api, 'getTrips');
  const apiGetCountriesSpy = jest.spyOn(api, 'getCountries');

  apiGetTripsSpy.mockReturnValue(Promise.resolve([]));
  apiGetCountriesSpy.mockReturnValue(Promise.resolve([]));
  buildEventSourceSpy.mockReturnValue({
    CLOSED: 0,
    CONNECTING: 0,
    OPEN: 0,
    dispatchEvent(event: Event): boolean {
      return false;
    },
    onerror: jest.fn(),
    onmessage: jest.fn(),
    onopen: jest.fn(),
    readyState: 0,
    url: '',
    withCredentials: false,
    addEventListener(
      type: any,
      listener: any,
      options?: boolean | AddEventListenerOptions
    ): void {},
    close(): void {},
    removeEventListener(
      type: any,
      listener: any,
      options?: boolean | EventListenerOptions
    ): void {}
  });
}

test('Render main app header', () => {
  mockApiFunctions();

  render(<App />);

  const mainHeader = screen.getByText(/Top seasonal countries/i);
  expect(mainHeader).toBeInTheDocument();
})

test('Test dark mode', () => {
  mockApiFunctions();

  render(<App />);

  const switchColorMode = () => {
    const switchColorModeButton = screen.getByTestId('switch-color-mode');
    switchColorModeButton.click();
  }

  let pageBackground = screen.getByTestId('page-background');

  const defaultTheme = DEFAULT_COLORS_MODE === 'light' ? lightTheme : darkTheme;

  expect(pageBackground).toHaveStyle(`background-color: ${defaultTheme.palette.background.paper}`)

  switchColorMode();

  pageBackground = screen.getByTestId('page-background');
  const themeAfterFirstChange = DEFAULT_COLORS_MODE === 'light' ? darkTheme : lightTheme;
  expect(pageBackground).toHaveStyle(`background-color: ${themeAfterFirstChange.palette.background.paper}`)

});
  
import { Accordion, AccordionSummary, Button, Paper, Typography } from '@mui/material';
import { addNewRandomTrip, addNewRandomCountry, removeLastTrip as removeLastTripApi, removeLastCountry as removeLastCountryApi } from '../Api/Api';
import React from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useAlert } from 'react-alert';


function DevelopmentActions() {

  const alert = useAlert();

  const generateRandomTrip = () => {
    addNewRandomTrip().then(res => {
      alert.success('Randomly generated Trip has been added');
    }).catch(err =>{
      alert.error('Error occured while trying to add a random trip');
    })
  }

  const generateRandomCountry = () => {
    addNewRandomCountry().then(res => {
      alert.success('Randomly generated Country has been added');
    }).catch(err =>{
      alert.error('Error occured while trying to add a random country');
    })
  }

  const removeLastTrip = () => {
    removeLastTripApi().then(res => {
      alert.success('Last Trip has been deleted');
    }).catch(err =>{
      alert.error('Couldn\'t delete the Trip');
    })
  }

  const removeLastCountry = () => {
    removeLastCountryApi().then(res => {
      alert.success('Last Country has been deleted');
    }).catch(err =>{
      alert.error('Couldn\'t delete the Country');
    })
  }

  return (
    <>{
      process.env.NODE_ENV !== 'production'
      && <Accordion
        sx={{
          position: 'sticky',
          height: 0,
          width: '300px',
          zIndex: 100,
          top: '4rem'
        }}
        disableGutters
      >
        <Paper sx={{borderRadius: 0}}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="development-actions-header"
            id="development-actions-header"
          >
            <Typography>Development HTTP actions</Typography>
          </AccordionSummary>
        </Paper>

        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'column',
            padding: '1rem',
            gap: 1,
            borderRadius: 0
          }}
        >
          <Button variant='contained' onClick={() => generateRandomTrip()}>Generate random trip</Button>
          <Button variant='contained' onClick={() => generateRandomCountry()}>Generate random country</Button>
          <Button variant='contained' color='error' onClick={() => removeLastTrip()}>Remove last trip</Button>
          <Button variant='contained' color='error' onClick={() => removeLastCountry()}>Remove last country</Button>
        </Paper>
      </Accordion>
    }

    </>
  );
}

export default DevelopmentActions;

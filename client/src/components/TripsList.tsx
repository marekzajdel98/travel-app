import { Box } from '@mui/system';
import React from 'react';
import { Typography } from '@mui/material';
import TripCard from './TripCard';
import { Trip } from '@trips/types';

interface Properties {
  title: string;
  trips: Trip[];
}

function TripsList(props: Properties) {
  return (
    <Box marginY={5}>
      <Typography variant='h4'>
       {props.title}
      </Typography>
      <Box sx={{display: 'flex', my: 2, flexWrap: 'wrap'}} rowGap={5} columnGap={3}>
        {props.trips.map(trip => 
          <TripCard
            key={`trip-card-${props.title}-${trip.id}`}
            trip={trip}
          />
        )}
      </Box>
    </Box>
  );
}

export default TripsList;

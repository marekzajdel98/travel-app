import { CardMedia, CardContent, Typography, Rating, Box, CardActionArea } from '@mui/material';
import Card from '@mui/material/Card';
import { formatter } from '../services/Currency';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Trip } from '@trips/types';

interface Properties {
  trip: Trip;
}

function TripCard(props: Properties) {
  const navigate = useNavigate();
  return (
   <Card sx={{  width: 280 }} elevation={8}>
      <CardActionArea onClick={() => navigate(`/trips/${props.trip.id}`)} sx={{height: '100%'}}>
        <CardMedia
          component='img'
          height='180'
          image={props.trip.detailImageUrl}
          alt='Trip image'
          sx={{
            transition: 'all .2s ease-in-out',
            ':hover': {
              transform: 'scale(1.1)'
            }
          }}
        />
        <CardContent>
          <Typography variant='body2' color='text.secondary'>{props.trip.countries?.length} countries, {props.trip.duration} days </Typography>
          <Typography gutterBottom variant='h5' component='div' textTransform='capitalize'>
            {props.trip.title}
          </Typography>
          <Box sx={{display: 'flex', alignItems: 'center'}}>
            <Rating name='trip-rating' sx={{fontSize: '1rem'}} defaultValue={props.trip.rating} precision={0.1} readOnly />
            <Typography ml={1} sx={{lineHeight: '10px', fontSize: '0.7rem', fontWeight: 600}}>{props.trip.rating}</Typography>
          </Box>
          <Box sx={{display: 'flex', gap: '0.5rem'}}>
            <Typography variant='body2' color='text.primary' fontWeight={600}>
              {formatter.format(props.trip.price)}
            </Typography>
            <Typography variant='body2' color='text.secondary' sx={{textDecoration: 'line-through'}}>
              {props.trip.originalPrice ? formatter.format(props.trip.originalPrice) : ''}
            </Typography>
          </Box>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default TripCard;

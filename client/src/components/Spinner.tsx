import { Box, CircularProgress } from '@mui/material';
import React from 'react';


function Spinner() {
  return (
    <Box py={10} sx={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
      <CircularProgress />
    </Box>
  );
}

export default Spinner;

import { AppBar, Box, Button, Divider, Drawer, IconButton, List, ListItem, ListItemButton, Toolbar, Tooltip } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';
import HomeIcon from '@mui/icons-material/Home';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
import GitHubIcon from '@mui/icons-material/GitHub';
import { ColorsMode } from '../services/Theme';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

interface Properties {
  setColorsMode: Function;
  colorsMode: ColorsMode;
}

function Navbar(props: Properties) {
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const navigate = useNavigate();
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const navItems = [
    {title: 'Contact', url: process.env.REACT_APP_LINKEDIN_URL, icon: <LinkedInIcon style={{marginLeft: 5}} color='primary'/>},
    {title: 'GitLab', url: process.env.REACT_APP_GITLAB_REPO_URL, icon: <GitHubIcon style={{marginLeft: 5}} color='primary'/>}
  ];

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center', display: 'flex', flexDirection: 'column' }}>
      <IconButton
        color="inherit"
        sx={{ display: 'flex', justifyContent: 'center' }}
        onClick={() => navigate('/')}
      >
          <HomeIcon/>
      </IconButton>
      <IconButton
        color="inherit"
        sx={{ display: 'flex', justifyContent: 'center' }}
        onClick={() => props.setColorsMode(props.colorsMode === 'light' ? 'dark' : 'light')}
        data-testid="switch-color-mode-mobile"
      >
        {props.colorsMode === 'dark' ? <LightModeIcon/> : <DarkModeIcon/>}
      </IconButton>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={`nav-item-${item.title}-mobile`} disablePadding>
            <a style={{color: 'white', textDecoration: 'none'}} href={item.url}>
              <ListItemButton sx={{ textAlign: 'center' }}>
                {item.title}
                {item.icon}
              </ListItemButton>
            </a>
          </ListItem>
        ))}
      </List>
    </Box>
  );


  return (
    <Box sx={{ display: 'flex' }}>
    <AppBar component="nav" color='secondary' enableColorOnDark>
      <Toolbar className='navbar'>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          sx={{ mr: 2, display: { sm: 'none' } }}
        >
          <MenuIcon />
        </IconButton>
        <Tooltip title="Homepage">
          <IconButton
            color="inherit"
            sx={{ display: { xs: 'none', sm: 'flex' }, justifyContent: 'center' }}
            onClick={() => navigate('/')}
          >
            <HomeIcon/>
          </IconButton>
        </Tooltip>
        <Tooltip title="Color mode">
          <IconButton
            color="inherit"
            sx={{ display: { xs: 'none', sm: 'flex' }, justifyContent: 'center' }}
            onClick={() => props.setColorsMode(props.colorsMode === 'light' ? 'dark' : 'light')}
            data-testid="switch-color-mode"
          >
            {props.colorsMode === 'dark' ? <LightModeIcon/> : <DarkModeIcon/>}
          </IconButton>
        </Tooltip>
        <Box sx={{ display: { xs: 'none', sm: 'block' }, marginLeft: 'auto' }}>
          {navItems.map((item) => (
            <a key={`nav-item-${item.title}`}style={{color: 'white', textDecoration: 'none'}} href={item.url}>
              <Button>
                {item.title}
                {item.icon}
              </Button>
            </a>
          ))}
        </Box>
      </Toolbar>
    </AppBar>
    <Box component="nav">
      <Drawer
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true,
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: '240px' },
        }}
      >
        {drawer}
      </Drawer>
    </Box>
    <Toolbar />
  </Box>
  );
}

export default Navbar;

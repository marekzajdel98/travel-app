import { Grid, List, ListItem, Paper, Typography } from '@mui/material';
import React from 'react';
import { generateRandomNumber } from '../utils/helpers';

const MAX_LINKS_AMOUNT_IN_SECTION = 10;

function Footer() {

  const generateTestLinks = (key: string) => {
    const links = [];
    for (let i=0; i<generateRandomNumber(MAX_LINKS_AMOUNT_IN_SECTION); i++) {
          links.push(<ListItem key={`footer-link-${key}-${i}`} sx={{px: 0}}><a href={process.env.REACT_APP_LINKEDIN_URL} style={{textDecoration: 'none'}}>Test link</a></ListItem>)
    }
    return links;
  }

  return (
    <Paper
      component='footer'
      sx={{
        marginTop: '5rem',
        bottom: 0,
        color: '#707070',
        height: 'auto',
        left: 0,
        position: 'relative',
        fontSize: 'small',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        paddingY: '5rem'
      }}
      elevation={10}
    >
      <Grid container spacing={2} sx={{width: '50%'}}>
        <Grid item xs={12} md={3}>
          <Typography fontWeight={600}>Company</Typography>
          {generateTestLinks('company')}
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography fontWeight={600}>Explore</Typography>
          {generateTestLinks('explore')}
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography fontWeight={600}>Policies</Typography>
          {generateTestLinks('policies')}
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography fontWeight={600}>Help</Typography>
          <List sx={{ width: '100%', maxWidth: 360}}>
            {generateTestLinks('help')}
          </List>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Footer;

import { Box } from '@mui/system';
import { getCountries } from '../../../Api/Api';
import React, { useEffect, useState } from 'react';
import { Card, CardActionArea, CardContent, CardMedia, Typography } from '@mui/material';
import EastIcon from '@mui/icons-material/East';
import { useNavigate } from 'react-router-dom';
import { TripCountry } from '@trips/types';
import { buildEventSource } from '../../../utils/helpers';
import { useAlert } from 'react-alert';

function TopCountries() {
  const [topCountries, setTopCountries] = useState<TripCountry[]>([]);

  const alert = useAlert();

  useEffect(() => {
    getCountries().then(res => {
      setTopCountries(res);
    }).catch(err => {
      alert.error('Error occured while trying to fetch top seasonal countries');
    })

    const events = buildEventSource('http://localhost:3004/countries-sse-handshake');

    events.onmessage = (event) => {
      const countries = JSON.parse(event.data).countries;
      if (countries) setTopCountries(countries);
    };

    return () => events.close();
  }, [alert])

  const navigate = useNavigate();

  return (
    <Box boxShadow={10} sx={{ overflow: 'hidden', width: '100%', minHeight: '600px', display: 'flex', position: 'relative', flexWrap: 'wrap'}}>
      {topCountries.map((country, index) => (
        <Card key={`top-country-${index}`} sx={{width: '100%', borderRadius: 0, minWidth: '300px', flex: '1 1'}}>
          <CardActionArea sx={{height: '750px'}} onClick={() => navigate(`/countries/${country.id}`)}>
            <CardMedia
              component='img'
              height='100%'
              image={country.listImageUrl}
              alt='green iguana'
            />
            <CardContent>
              <Typography
                gutterBottom
                variant='h3'
                component='div'
                className='noselect text-contrast-shadow'
                sx={{
                  color: 'white',
                  position: 'absolute',
                  bottom: 10,
                }}
               >
                {country.country}
              </Typography>
              <Typography
                gutterBottom
                variant='body1'
                component='p'
                className='noselect text-contrast-shadow'
                sx={{
                  color: 'text.secondary',
                  position: 'absolute',
                  bottom: 0,
                  maxHeight: '20px',
                  textOverflow: 'ellipsis',
                  maxWidth: '80%',
                  overflow: 'hidden',
                  whiteSpace: 'nowrap',
                }}
              >
                {country?.description}
              </Typography>
              <EastIcon sx={{color: 'white', position: 'absolute', bottom: '1rem', right: '1rem', fontSize: '3rem'}}></EastIcon>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
      <Typography
        variant='h1'
        component='h1'
        className='noselect text-contrast-shadow centered-absolute-text'
        sx={{top: '10%'}}
      >
        Top seasonal countries
      </Typography>
    </Box>
  );
}

export default TopCountries;

import { batchGetTrips } from '../../../Api/Api';
import React, { useEffect, useState } from 'react';
import TripsList from '../../../components/TripsList';
import { Trip } from '@trips/types';
import { getRecentlyViewedTripIds } from '../../../services/RecentlyViewedTrips';
import { useAlert } from 'react-alert';

function RecentlyViewedTrips() {
  const [recentlyViewedTrips, setRecentlyViewedTrips] = useState<Trip[]>([]);

  const alert = useAlert();

  useEffect(() => {
    const recentlyViewedTripIds = getRecentlyViewedTripIds();
    batchGetTrips(recentlyViewedTripIds).then(res => {
      setRecentlyViewedTrips(res);
    }).catch(err => {
      alert.error('Error occured while trying to fetch recently viewed trips');
    })
  }, [alert]);

  return (
    <>
      {recentlyViewedTrips.length
        ? <TripsList trips={recentlyViewedTrips} title='Recently viewed trips'/>
        : ''
      }
    </>

  );
}

export default RecentlyViewedTrips;

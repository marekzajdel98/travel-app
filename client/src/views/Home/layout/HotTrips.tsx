import { Trip } from '@trips/types';
import { getTrips } from '../../../Api/Api';
import React, { useEffect, useState } from 'react';
import TripsList from '../../../components/TripsList';
import { buildEventSource } from '../../../utils/helpers';
import { useAlert } from 'react-alert';

function HotTrips() {
  const [hotTrips, setHotTrips] = useState<Trip[]>([]);

  const alert = useAlert();

  useEffect(() => {
    getTrips().then(res => {
      setHotTrips(res);
    }).catch(err => {
      alert.error('Error occured while trying to fetch trips');
    })

    const events = buildEventSource('http://localhost:3004/trips-sse-handshake');

    events.onmessage = (event) => {
      const trips = JSON.parse(event.data)?.trips;
      if (trips) setHotTrips(trips);
    };

    return () => events.close();
  }, [alert]);

  return (
    <TripsList trips={hotTrips} title='Hot trips'/>
  );
}

export default HotTrips;

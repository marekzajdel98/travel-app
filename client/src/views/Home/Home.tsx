import { Box } from '@mui/material';
import React from 'react';
import HotTrips from './layout/HotTrips';
import RecentlyViewedTrips from './layout/RecentlyViewedTrips';
import TopCountries from './layout/TopCountries';
import DevelopmentActions from '../../components/DevelopmentActions';


function Home() {
  return (
    <>
      <DevelopmentActions />
      <TopCountries/>
      <Box px='10%' py='3rem'>
        <HotTrips></HotTrips>
        <RecentlyViewedTrips/>
      </Box>
    </>
  );
}

export default Home;

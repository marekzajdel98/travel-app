import { Card, CardMedia, Grid, ImageList, ImageListItem, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { getTrip } from '../../Api/Api';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Trip } from '@trips/types';
import { addNewRecentlyViewedTrip } from '../../services/RecentlyViewedTrips';
import Spinner from '../../components/Spinner';
import { useAlert } from 'react-alert';

function TripDetail() {
  const [trip, setTrip] = useState<Trip | null>(null);

  const { id } = useParams();
  const alert = useAlert();

  useEffect(() => {
    if (!id) return;

    getTrip(id).then(res => {
      setTrip(res);
      addNewRecentlyViewedTrip(id);
    }).catch(err => {
      alert.error('Error occured while trying to fetch trip details');
    })
  }, [id, alert])

  return (
    <Box>      
      {trip
      ? <Grid container spacing={2} paddingX={'5%'} paddingY='3rem'>
        <Grid height='500px' item xs={12} md={6}> 
          <Card sx={{width: '100%', height: '100%'}} elevation={5}>
            <CardMedia
              component='img'
              height='100%'
              image={trip?.detailImageUrl}
            />
          </Card>
        </Grid>
        <Grid height='500px' sx={{overflow: 'hidden'}} item xs={12} md={5}>
          <Typography variant='h3'>
            {trip.title}
          </Typography>
          <Typography variant='body1'>
            {trip.description}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <ImageList cols={10} sx={{display: 'flex', flexWrap: 'wrap'}}>
            {trip.overviewImages.map((image, index) => (
              <ImageListItem key={`overview-image-${index}`} sx={{width: '200px', height: '200px'}}>
                <img
                  src={image.imageUrl}
                  srcSet={image.imageUrl}
                  alt={image.title}
                  loading='lazy'
                />
              </ImageListItem>
            ))}
          </ImageList>
        </Grid>
      </Grid>
      : <Spinner />
      }
    </Box>
  );
}

export default TripDetail;

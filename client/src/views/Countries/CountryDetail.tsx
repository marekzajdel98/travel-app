import { Grid, Card, CardMedia, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Trip, TripCountry } from '@trips/types';
import { getCountry, getCountryTrips } from '../../Api/Api';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import TripsList from '../../components/TripsList';
import Spinner from '../../components/Spinner';
import { useAlert } from 'react-alert';

function CountryDetail() {
  const [country, setCountry] = useState<TripCountry | null>(null);
  const [countryTrips, setCountryTrips] = useState<Trip[]>([]);

  const { id } = useParams();
  const alert = useAlert();

  useEffect(() => {
    if (!id) return;

    getCountry(id).then(res => {
      setCountry(res);
    }).catch(err => {
      alert.error('Error occured while trying to fetch countries');
    })

    getCountryTrips(id).then(res => {
      setCountryTrips(res);
    }).catch(err => {
      alert.error('Error occured while trying to fetch trips for this country');
    })
  }, [id, alert])

  return (
    <Box>
      { country
      ? <Grid container spacing={2}>
        <Grid height='500px' item xs={12}> 
          <Card sx={{width: '100%', height: '100%', position: 'relative', borderRadius: 0}} elevation={10}>
            <CardMedia
              component='img'
              height='100%'
              image={country?.detailImageUrl}
            />
            <Typography
              variant="h1"
              className='noselect text-contrast-shadow centered-absolute-text'
            >
              {country.country}
            </Typography>
          </Card>
        </Grid>
        <Grid px={'10%'} py='2rem'>
          <Grid item xs={12}>
            <Typography variant="h4">
              {country.country}
            </Typography>
            <Typography variant='body2'>
              {country.description}
            </Typography>
          </Grid>
          {countryTrips.length > 0
            && <Grid item xs={12}>
            <TripsList title='Featured trips' trips={countryTrips}></TripsList>
          </Grid>
          }
        </Grid>
      </Grid>
      : <Spinner/>
      }
    </Box>
  );
}

export default CountryDetail;

import { Trip, TripCountry } from '@trips/types';
import axios from 'axios';

const httpClient = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL
});

export async function getCountries(): Promise<TripCountry[]> {
  return (await httpClient.get('countries'))?.data;
}

export async function getTrips(country?: string): Promise<Trip[]> {
  return (await httpClient.get(`trips${country ? '?country=' + country : ''}`))?.data;
}

export async function getTrip(id: string): Promise<Trip> {
  return (await httpClient.get(`trips/${id}`)).data;
}

export async function batchGetTrips(ids: string[]): Promise<Trip[]> {
  if (ids.length === 0) return [];
  const trips: Trip[] = (await httpClient.get(`trips?ids=${ids.join('&ids=')}`)).data
  return trips;
}

export async function getCountryTrips(countryId: string): Promise<Trip[]> {
  return (await httpClient.get(`trips?country_id=${countryId}`)).data;
}

export async function getCountry(id: string): Promise<TripCountry> {
  return (await httpClient.get(`countries/${id}`)).data;
}

export async function addNewRandomTrip() {
  return await httpClient.post(`trips`);
}

export async function addNewRandomCountry() {
  return await httpClient.post(`countries`);
}

export async function removeLastTrip() {
  return await httpClient.delete(`trips`);
}

export async function removeLastCountry() {
  return await httpClient.delete(`countries`);
}
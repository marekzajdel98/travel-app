import React, { useState } from 'react';
import './App.css';
import Home from './views/Home/Home';
import { ThemeProvider } from '@mui/material/styles';
import { lightTheme, darkTheme, ColorsMode, DEFAULT_COLORS_MODE } from './services/Theme';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import TripDetail from './views/Trips/TripDetail';
import Footer from './layout/Footer';
import CountryDetail from './views/Countries/CountryDetail';
import Navbar from './layout/Navbar';
import { Paper } from '@mui/material';
import { Provider as AlertProvider } from 'react-alert'
import AlertTemplate from './utils/react-alert/AlertTemplate';
import { REACT_ALERT_OPTIONS } from './utils/react-alert/options';

function App() {
  const [colorsMode, setColorsMode] = useState<ColorsMode>(DEFAULT_COLORS_MODE);

  return (
    <AlertProvider template={AlertTemplate} {...REACT_ALERT_OPTIONS}>
      <ThemeProvider theme={colorsMode === 'light' ? lightTheme : darkTheme}>
        <BrowserRouter>
          <Paper data-testid="page-background">
            <Navbar setColorsMode={setColorsMode} colorsMode={colorsMode} />
              <Routes>
                <Route index element={<Home />} />
                <Route path='/trips/:id' element={<TripDetail />} />
                <Route path='/countries/:id' element={<CountryDetail />} />
                <Route path='*' element={<Navigate replace to="/" />} />
              </Routes>
            <Footer/>
          </Paper>
        </BrowserRouter>
      </ThemeProvider>
    </AlertProvider>
  );
}

export default App;

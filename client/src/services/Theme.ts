import { createTheme } from '@mui/material';

const baseTypography = {
  h1: {
    fontSize: '5.5rem',
    '@media (max-width:600px)': {
      fontSize: '3rem',
    }
  }
};

const basePalette = {
  text: {
    secondary: '#a1a1a1'
  },
  secondary: {
    main: '#282a36'
  }
};

export const lightTheme = createTheme({
  palette: basePalette,
  typography: baseTypography
});

export const darkTheme = createTheme({
  palette: {
    ...basePalette,
    mode: 'dark'
  },
  typography: baseTypography
});

export type ColorsMode = 'light' | 'dark';

export const DEFAULT_COLORS_MODE: ColorsMode = 'dark';
import { moveArrayElementToTheBegginning } from '../utils/helpers';

const MAX_RECENTLY_VIEWED_TRIPS = 5;
const RECENTLY_VIEWED_TRIPS_LOCAL_STORAGE_KEY = 'recently-viewed-trips';

export function addNewRecentlyViewedTrip(tripId: string) {
  let trips = getRecentlyViewedTripIds();

  const tripIndex = trips.indexOf(tripId);
  const tripAlreadyInLocalStorage = tripIndex !== -1;

  if (tripAlreadyInLocalStorage)
    trips = moveArrayElementToTheBegginning(trips, tripIndex);
  else if (trips.length >= MAX_RECENTLY_VIEWED_TRIPS) {
    trips.pop();
    trips.unshift(tripId)
  } else
    trips.unshift(tripId)

  localStorage.setItem(RECENTLY_VIEWED_TRIPS_LOCAL_STORAGE_KEY, JSON.stringify(trips));
}

export function getRecentlyViewedTripIds(): string[] {
  const stringifiedTrips = localStorage.getItem(RECENTLY_VIEWED_TRIPS_LOCAL_STORAGE_KEY);
  const trips = stringifiedTrips ? JSON.parse(stringifiedTrips) : [];
  return trips;
}